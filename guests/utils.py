from django.core.exceptions import ValidationError
from django import forms


class OptionalChoiceWidget(forms.MultiWidget):
    def decompress(self,value):
        #this might need to be tweaked if the name of a choice != value of a choice
        if value: #indicates we have a updating object versus new one
            if value in [x[0] for x in self.widgets[0].choices]:
                 return [value,''] # make it set the pulldown to choice
            else:
                 return ['other',value] # keep pulldown to blank, set freetext
        return ['',''] # default for new object

class OptionalChoiceField(forms.MultiValueField):
    def __init__(self, choices, max_length=80, required=False, *args, **kwargs):
        '''
        sets the two fields as not required but will enforce that (at least) one is set
        in compress
        '''
        fields = (forms.ChoiceField(choices=choices,required=False,
                                    widget=forms.Select(attrs={'class': 'optional-choice-select'})),
                  forms.CharField(required=False,
                                  widget=forms.TextInput(attrs={'class': 'optional-choice-text'})))
        self.widget = OptionalChoiceWidget(widgets=[f.widget for f in fields])
        self.required = False
        super(OptionalChoiceField,self).__init__(required=False,fields=fields,*args,**kwargs)

    def compress(self,data_list):
        '''
        return the choicefield value if selected or charfield value (if both empty, will
        throw exception
        '''
        if not data_list:
            if self.required:
                raise ValidationError('Need to select choice or enter text for this field')
            return ''
        if data_list[0] == 'other':
            return data_list[1]
        return data_list[0]
