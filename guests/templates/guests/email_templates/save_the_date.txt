Dear {{ party_names }}

Save the date for {{ couple }}'s wedding! 10 April 2021 at De Uijlenes,
Gansbaai, South Africa. There will be accommodation available, with arrival on
Friday 9 April and departure on Sunday 10 April. More details will follow in due
course. Directions are available on the website - {{ site_url }}.

If you are unable to make it please let us know at {{ rsvp_address }} or reply
to this email.

Hope to see you there!
{{ couple }}
