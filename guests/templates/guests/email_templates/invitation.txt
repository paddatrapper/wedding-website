Dear {{ party_names }},

You are invited to {{ couple }}'s wedding! It is starting at 16:00 on Saturday,
10 April 2021. Please RSVP by 13 March 2021 on the website - {% url 'invitation'
invitation_id %}. Directions and more details are available on the website -
{{ site_url }}.

Ceremony
========

The ceremony is kicking off at 16:00 on Saturday, 10 April 2021. The dress code is
semi-formal. Please be aware that it is outside and there is grass, so your stilettos
may sink deep.

There will be some wine, a glass of champagne and water available at the
reception, but if you would like anything else during the night, a cash bar will be
available.

Friday Braai
============

There will be a bring and braai on the Friday night starting at 18:00. A selection of sides
will be provided, however we request that you bring your own meat and drinks. You will
need to arrive before 18:00, as the venue's COVID-19 protocols require all guests are
screened before coming onto the property and the staff leave at 18:00.

Accommodation
=============

There is accommodation on site with a variety of room sizes available. The accommodation
has a communal fully-equipped kitchen, WiFi, a hot tub and has easy access to a beautiful
swimming dam. It is available for R240 per person for the weekend (Friday and Saturday
night) or R100 p/p for only Saturday night. If you wish to make use of the
accommodation, please indicate such on your RSVP.

Live Streaming
==============

For all of those who will unfortunately not be able to join us on our wedding day, we
have arranged to live-stream the most important parts. It will be made available on the
website closer to the time. Please see {{ site_url }}#other-stuff for more details.

COVID-19
========

Unfortunately Corona Virus is still very much a harsh reality around
the world. We are bound by South African regulations. Thus are
following all guidelines instituted by De Uljlenes, in an effort to
protect all attending.

*As such please take note of the following rules:*

  - You will be required to wear a mask at all times
  - Sanitizer will be made available throughout the venue and
    accommodation for your frequent use
  - You will be required to screen at reception when you arrive at
    the venue. *Note: the screening operates until 18:00.*
    Thus, please allow for this if you are joining us for the
    braai on Friday evening, as you will not be able to arrive after
    18:00.
  - If you present with any symptoms of COVID-19 or have been in contact
    with a positive case in the last 14 days, please self-isolate. You
    are welcome to join us on our special day via livestream.

More information on the weekend can be found on the website - {{ site_url }}

Hope to see you there!
{{ couple }}
