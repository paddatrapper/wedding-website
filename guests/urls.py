from django.conf.urls import url

from guests.views import GuestListView, test_email, save_the_date, export_guests, \
        invitation, invitation_email_preview, invitation_email_test, rsvp_confirm, \
        dashboard, update_email_1, update_email_2, thank_you_email_preview

urlpatterns = [
    url(r'^guests/$', GuestListView.as_view(), name='guest-list'),
    url(r'^dashboard/$', dashboard, name='dashboard'),
    url(r'^guests/export$', export_guests, name='export-guest-list'),
    url(r'^invite/(?P<invite_id>[\w-]+)/$', invitation, name='invitation'),
    url(r'^invite-email/(?P<invite_id>[\w-]+)/$', invitation_email_preview, name='invitation-email'),
    url(r'^invite-email-test/(?P<invite_id>[\w-]+)/$', invitation_email_test, name='invitation-email-test'),
    url(r'^save-the-date/$', save_the_date, name='save-the-date'),
    url(r'^update-email-1/$', update_email_1, name='update-email-1'),
    url(r'^update-email-2/$', update_email_2, name='update-email-2'),
    url(r'^email-test/(?P<template_id>[\w-]+)/$', test_email, name='test-email'),
    url(r'^rsvp/confirm/(?P<invite_id>[\w-]+)/$', rsvp_confirm, name='rsvp-confirm'),
    url(r'^thank-you-email/(?P<invite_id>[\w-]+)/$', thank_you_email_preview, name='thank-you-email'),
]
