from guests.models import Party
from guests.emails import send_email
from django.conf import settings
from django.utils import timezone

EMAIL_TEMPLATE = 'guests/email_templates/thank_you.html'

def send_all_thankyous(test_only, mark_as_sent):
    to_send_to = Party.in_default_order().filter(send_thank_you=True, thank_you_sent=None).exclude(is_attending=False)
    for party in to_send_to:
        send_thank_you_email(party, test_only=test_only)
        if mark_as_sent:
            party.thank_you_sent = timezone.now()
            party.save()

def get_context(party):
    return {
        'main_image': 'thank_you.png',
        'title': "Jess and Kyle's Wedding - Thank you",
        'personal_message': party.personal_message,
        'thank_you_id': party.invitation_id,
        'share_link': settings.PHOTO_SHARE_LINK,
    }

def send_thank_you_email(party, test_only=False, recipients=None):
    send_email(party, 'thank_you', test_only, recipients, get_context(party))

def clear_all_thankyous():
    print('clear')
    for party in Party.objects.filter(send_thank_you=True).exclude(thank_you_sent=None):
        party.thank_you_sent = None
        print("resetting {}".format(party))
        party.save()
