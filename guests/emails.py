import os
from copy import copy
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from email.mime.image import MIMEImage
from guests.models import Party

EMAIL_TEMPLATE = 'guests/email_templates/{template}.html'
EMAIL_TEXT_TEMPLATE = 'guests/email_templates/{template}.txt'

INVITATION_CONTEXT_MAP = {
    'title': "Jess and Kyle's Wedding - Update",
    'header_filename': 'hearts.png',
    'main_color': '#ffffff',
    'font_color': '#8a2432',
}

def get_context():
    context = copy(INVITATION_CONTEXT_MAP)
    context['site_url'] = settings.WEDDING_WEBSITE_URL
    context['couple'] = settings.BRIDE_AND_GROOM
    context['livestream_link'] = settings.YOUTUBE_LIVE_STREAM
    return context

def send_email(party, template, test_only=False, recipients=None, extra_context={}):
    if recipients is None:
        recipients = party.guest_emails
    if not recipients:
        print ('===== WARNING: no valid email addresses found for {} ====='.format(party))
        return

    context = get_context()
    context.update(extra_context)
    context['email_mode'] = True
    party_names = party.guest_set.values('first_name')
    party_names = [p['first_name'] for p in party_names]
    if len(party_names) > 1:
        party_names = ', '.join(party_names[:-1]) + ' and ' + party_names[-1]
    else:
        party_names = party_names[0]
    context['party_names'] = party_names
    template_html = render_to_string(EMAIL_TEMPLATE.format(template=template), context=context)
    template_text = render_to_string(EMAIL_TEXT_TEMPLATE.format(template=template), context=context)
    subject = "Jess and Kyle's Wedding - Update"
    # https://www.vlent.nl/weblog/2014/01/15/sending-emails-with-embedded-images-in-django/
    msg = EmailMultiAlternatives(subject, template_text,
                                 settings.DEFAULT_WEDDING_FROM_EMAIL, recipients,
                                 cc=settings.WEDDING_CC_LIST,
                                 reply_to=[settings.RSVP_WEDDING_REPLY_EMAIL])
    msg.attach_alternative(template_html, "text/html")
    msg.mixed_subtype = 'related'
    for filename in (context['header_filename'], ):
        attachment_path = os.path.join(os.path.dirname(__file__), 'static', 'invitation', 'images', filename)
        with open(attachment_path, "rb") as image_file:
            msg_img = MIMEImage(image_file.read())
            msg_img.add_header('Content-ID', '<{}>'.format(filename))
            msg.attach(msg_img)

    print ('sending email to {} ({})'.format(party.name, ', '.join(recipients)))
    if not test_only:
        msg.send()


def send_all(test_only, template):
    to_send_to = Party.in_default_order().filter(is_attending=True)
    for party in to_send_to:
        send_email(party, template, test_only=test_only)

