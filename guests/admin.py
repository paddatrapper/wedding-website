from django.contrib import admin
from .models import Guest, Party


class GuestInline(admin.TabularInline):
    model = Guest
    fields = ('first_name',
              'last_name',
              'email',
              'is_attending',
              'dietary_restrictions',
              'attending_braai',
              'needs_accomodation',
              'is_child')
    readonly_fields = ('first_name', 'last_name', 'email')


class PartyAdmin(admin.ModelAdmin):
    list_display = ('name', 'save_the_date_sent', 'invitation_sent',
                    'invitation_opened',
                    'is_invited',
                    'is_attending')
    list_filter = ('is_invited',
                   'is_attending',
                   'invitation_opened')
    inlines = [GuestInline]


class GuestAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'party', 'email', 'is_attending', 'is_child', 'dietary_restrictions', 'attending_braai', 'needs_accomodation')
    list_filter = ('is_attending', 'is_child', 'dietary_restrictions', 'party__is_invited', 'party__rehearsal_dinner', 'attending_braai', 'needs_accomodation')


admin.site.register(Party, PartyAdmin)
admin.site.register(Guest, GuestAdmin)
