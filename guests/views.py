import base64
from collections import namedtuple
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.forms import formset_factory
from django.urls import reverse
from django.db.models import Count, Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import ListView
from guests import csv_import
from guests.invitation import get_invitation_context, INVITATION_TEMPLATE, \
        guess_party_by_invite_id_or_404, send_invitation_email
from guests.models import Guest, Party
from guests.save_the_date import get_save_the_date_context, \
        send_save_the_date_email, SAVE_THE_DATE_TEMPLATE, \
        SAVE_THE_DATE_CONTEXT_MAP
from guests.forms import IndividualRSVPForm, RSVPFormset
from guests import emails, thankyou


class GuestListView(ListView):
    model = Guest


@login_required
def export_guests(request):
    export = csv_import.export_guests()
    response = HttpResponse(export.getvalue(), content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=all-guests.csv'
    return response


@login_required
def dashboard(request):
    parties_with_pending_invites = Party.objects.filter(
        is_invited=True, is_attending=None
    ).order_by('name')
    parties_with_unopen_invites = parties_with_pending_invites.filter(invitation_opened=None)
    parties_with_open_unresponded_invites = parties_with_pending_invites.exclude(invitation_opened=None)
    attending_guests = Guest.objects.filter(is_attending=True)
    guests_without_meals = attending_guests.filter(
        is_child=False
    ).filter(
        Q(dietary_restrictions__isnull=True) | Q(dietary_restrictions='')
    ).order_by('first_name')
    meal_breakdown = attending_guests.exclude(dietary_restrictions=None).values('dietary_restrictions').annotate(count=Count('*'))
    return render(request, 'guests/dashboard.html', context={
        'guests': Guest.objects.filter(is_attending=True).count(),
        'possible_guests': Guest.objects.filter(party__is_invited=True).exclude(is_attending=False).count(),
        'not_coming_guests': Guest.objects.filter(is_attending=False).count(),
        'pending_invites': parties_with_pending_invites.count(),
        'pending_guests': Guest.objects.filter(party__is_invited=True, is_attending=None).count(),
        'guests_without_meals': guests_without_meals,
        'parties_with_unopen_invites': parties_with_unopen_invites,
        'parties_with_open_unresponded_invites': parties_with_open_unresponded_invites,
        'unopened_invite_count': parties_with_unopen_invites.count(),
        'total_invites': Party.objects.filter(is_invited=True).count(),
        'meal_breakdown': meal_breakdown,
    })

def invitation(request, invite_id):
    party = guess_party_by_invite_id_or_404(invite_id)
    if party.invitation_opened is None:
        # update if this is the first time the invitation was opened
        party.invitation_opened = timezone.now()
        party.save()
    guests = party.ordered_guests.all()
    RSVPForm = formset_factory(IndividualRSVPForm,
                               extra=party.ordered_guests.count(),
                               formset=RSVPFormset)
    if request.method == 'POST':
        formset = RSVPForm(request.POST, form_kwargs={'party': guests})
        if formset.is_valid():
            for form in formset.cleaned_data:
                guest = party.guest_set.get(pk=form['guest_id'])
                guest.is_attending = form['is_attending']
                guest.needs_accomodation = form['needs_accomodation']
                guest.attending_braai = form['attending_friday_braai']
                guest.dietary_restrictions = form['dietary_restrictions'].lower()
                if form['guest_name'] != '':
                    name = form['guest_name'].split(' ')
                    guest.first_name = name[0]
                    guest.last_name = ' '.join(name[1:]).strip()
                guest.save()
            party.is_attending = party.any_guests_attending
            party.save()
            return HttpResponseRedirect(reverse('rsvp-confirm', args=[invite_id]))
    else:
        formset = RSVPForm(form_kwargs={'party': guests})
    return render(request, template_name='guests/invitation.html', context={
        'bride_and_groom': settings.BRIDE_AND_GROOM,
        'formset': formset,
    })

def rsvp_confirm(request, invite_id=None):
    party = guess_party_by_invite_id_or_404(invite_id)
    return render(request, template_name='guests/rsvp_confirmation.html', context={
        'party': party,
        'support_email': settings.DEFAULT_WEDDING_REPLY_EMAIL,
        'bride_and_groom': settings.BRIDE_AND_GROOM,
    })


def invitation_email_preview(request, invite_id):
    party = guess_party_by_invite_id_or_404(invite_id)
    context = get_invitation_context(party)
    return render(request, INVITATION_TEMPLATE, context=context)


@login_required
def invitation_email_test(request, invite_id):
    party = guess_party_by_invite_id_or_404(invite_id)
    send_invitation_email(party, recipients=[settings.DEFAULT_WEDDING_TEST_EMAIL])
    return HttpResponse('sent!')

def save_the_date(request):
    context = get_save_the_date_context()
    context['email_mode'] = False
    return render(request, SAVE_THE_DATE_TEMPLATE, context=context)

def update_email_1(request):
    context = emails.get_context()
    context['email_mode'] = False
    return render(request, emails.EMAIL_TEMPLATE.format(template='details'),
                  context=context)

def update_email_2(request):
    context = emails.get_context()
    context['email_mode'] = False
    return render(request, emails.EMAIL_TEMPLATE.format(template='livestream'),
                  context=context)

@login_required
def test_email(request, template_id):
    context = get_save_the_date_context()
    send_save_the_date_email(context, [settings.DEFAULT_WEDDING_TEST_EMAIL])
    return HttpResponse('sent!')


def _base64_encode(filepath):
    with open(filepath, "rb") as image_file:
        return base64.b64encode(image_file.read())

def thank_you_email_preview(request, invite_id):
    party = guess_party_by_invite_id_or_404(invite_id)
    context = emails.get_context()#.update(thankyou.get_context(party))
    extra_context = thankyou.get_context(party)
    context.update(extra_context)
    return render(request, thankyou.EMAIL_TEMPLATE, context=context)
