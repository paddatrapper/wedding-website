from django import forms
from .utils import OptionalChoiceField
from .models import Guest

YES_NO_CHOICES = (
    (True, 'Yes'),
    (False, 'No'),
)

DIETARY_RESTRICTION_CHOICES = (
    ('none', 'None'),
    ('vegetarian', 'Vegetarian'),
    ('vegan', 'Vegan'),
    ('white', 'No red meat'),
    ('other', 'Other'),
)


class IndividualRSVPForm(forms.Form):
    guest_name = forms.CharField(required=False,
                                 label='Guest name (if different)',
                                 widget=forms.TextInput(attrs={
                                     'class': 'guest-name-field form-input'
                                 }))
    is_attending = forms.ChoiceField(choices=YES_NO_CHOICES,
                                     required=False,
                                     widget=forms.RadioSelect(attrs={
                                         'class': 'attending-radio form-check-input'
                                     }))
    dietary_restrictions = OptionalChoiceField(required=False,
                                               choices=DIETARY_RESTRICTION_CHOICES)
    needs_accomodation = forms.ChoiceField(choices=YES_NO_CHOICES,
                                           required=False,
                                           widget=forms.RadioSelect())
    attending_friday_braai = forms.ChoiceField(choices=YES_NO_CHOICES,
                                               required=False,
                                               widget=forms.RadioSelect())
    guest_id = forms.IntegerField(widget=forms.NumberInput(attrs={
        'class': 'hidden-field form-input'
    }))

    def __init__(self, *args, guest_index, party, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['guest_id'].initial = party[guest_index].pk
        self.guest = party[guest_index]

    def clean(self):
        is_attending = self.cleaned_data['is_attending']

        if is_attending == 'True' or is_attending == '':
            if self.cleaned_data['is_attending'] == '':
                self.add_error('is_attending', 'This field is required')
            if self.cleaned_data['needs_accomodation'] == '':
                self.add_error('needs_accomodation', 'This field is required')
            if self.cleaned_data['attending_friday_braai'] == '':
                self.add_error('attending_friday_braai', 'This field is required')
            guest_id = self.cleaned_data['guest_id']
            guest_name = self.cleaned_data['guest_name']
            guest = None
            try:
                guest = Guest.objects.get(pk=guest_id)
            except Guest.DoesNotExist:
                raise forms.ValidationError('Guest not found')
            if guest.name == 'Guest' and guest_name == '':
                self.add_error('guest_name', 'This field is required')
        return self.cleaned_data


class RSVPFormset(forms.BaseFormSet):
    def get_form_kwargs(self, index):
        kwargs = super().get_form_kwargs(index)
        kwargs['guest_index'] = index
        return kwargs
