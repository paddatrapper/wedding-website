from optparse import make_option
from django.core.management import BaseCommand
from guests import csv_import
from guests.invitation import send_all_invitations
from guests.emails import send_all


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--send',
            action='store_true',
            dest='send',
            default=False,
            help="Actually send emails"
        )
        parser.add_argument(
            'template',
            help="The template to send out"
        )

    def handle(self, *args, **options):
        send_all(test_only=not options['send'],
                             template=options['template'])
