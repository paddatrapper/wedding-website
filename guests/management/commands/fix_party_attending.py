from django.core.management import BaseCommand
from guests.models import Party

class Command(BaseCommand):
    def handle(Self, *args, **options):
        for party in Party.objects.all():
            if party.guest_set.filter(is_attending=None).count() == party.guest_set.count():
                party.is_attending = None
            else:
                party.is_attending = party.any_guests_attending
            party.save()

